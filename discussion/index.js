//JS ES6 updates
//ECMAScript - the technology that is used to create the languages such as Javascript

//exponent operation
	//pre-es6
const firstNum = Math.pow(8, 2);
	console.log(firstNum);


	//es6
const secondNum = 8**2;
	console.log(secondNum);


//Template Literals - allows writing of strings without the use of concat operator; helps in terms of readability of the codes as well as the efficiency of work.
/*
 -create a anme variable and store a name of a person
 -create a message variable, greeting the person and welcoming him/her in the programming field log in the console the message
*/
let name = "John";
message = `Hello ${name}! Welcome to the programming field`;
console.log (message);


//Multiline
const anotherMessage = `
${name} attended a math competition.
He won it by solving the problem 8**2 with the answer of ${secondNum}
`
console.log(anotherMessage);



//computation inside the template literals
const interestRate = .10;
const principal = 1000;
console.log(`The interest on your savings is ${principal * (1 + interestRate)}`)



//Arrat Destructuring - allows unpacking elements in arrays into distinct variables; allows naming of array elements with vailables instead of using index numbers; helps with the code readability and coding efficiency


//pre-es6
fullName = ["Juan", "dela", "Cruz"];
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

//es6
const [ firstName, middleName, lastName ] = fullName;
console.log(firstName);
console.log(middleName);
console.log(lastName);



//Object Destructuring - allows unpacking elements in object into distinct variables; shortens syntax for accessing the objects
let woman = {
	givenName: "Marites",
	maidenName: "Anyare",
	familyName: "Sayo"
}

//pre-es6
console.log(woman.givenName);
console.log(woman.maidenName);
console.log(woman.familyName);
console.log(`Hello ${woman.givenName} ${woman.maidenName} ${woman.familyName}!`)


const {givenName, maidenName, familyName} = woman;
console.log(givenName);
console.log(maidenName);
console.log(familyName);
console.log(`Hello ${givenName} ${maidenName} ${familyName}!`);



//Arrow Function
/*
compact alternative syntax
*/
//pre-es6
/*
	Parts:
	declaration
	functionName
	parameters
	statements
	invoke/call the function
*/
function persona(firstName, middleInitial, lastName) {
	console.log(firstName);
	console.log(middleInitial);
	console.log(lastName);
	console.log (`${firstName} ${middleInitial} ${lastName}`)
};

persona("Aya","C.", "Kat");

//es6
const printFName=(fname, mname, lname) =>{
	console.log(fname, mname, lname);
}
printFName("Will", "D.", "Smith");


const student = ['John', 'Jane','Joe'];

 // pre-es6
 student.forEach (
 	function (element) {
 		console.log(element);
 	}
 );

 //es6
 	//if you have 2 or more parameters, encolse them inside a pair of prenthesis
 student.forEach(x=> console.log(x));

//(Arrow Functions) Implicit Return Statement - return statement/s are omitted because even without them, JS implicitly add them for the result of the function
//pre-es6 (function that adds two numbers)
 function addNumbers(x,y){
 	return x + y
 };
 let total = addNumbers(1,2)
 console.log(total);

 //es6

 const adds = (x,y) => x+y;
 // the code above actually runs as const adds = (x,y) => return x+y;
 let totals = adds(4,6);
 console.log(totals);

 //(Arrow Function) Default Function Argument Vale
/*
	provides a defaule value if no parameters are included/specified once the function has been invoked
 */
const greet = (name = "User") => {
	return `Good morning, ${name}`
}
console.log(greet());

console.log(greet("Anna"));


function car(name, brand, year){
	this.name = name
	this.brand = brand
	this.year = year
};
const car1 = new car("Honda", "Vios", 2020);
console.log(car1);

// class construction
	//class keyword declares the creation of a "car" object
	//constructor keyword - psecial method of creating/ initializing an object for the "car" class
	//this - sets the properties that are included to the "car" class
class cars {
	constructor (brandName, carName, mfy) {
		this.brandName = brandName,
		this.carName = carName,
		this.mfy = mfy
	}
};

const cars1 =  new cars("Ford", "Ranger Raptor", 2021);
console.log(cars1);

const cars2 = new cars();
console.log(cars2);
cars2.brandName = "Toyota",
cars2.carName = "Fortuner",
cars2.mfy = 2020
console.log(cars2);


let num = 10;
(num<=0)? console.log(true) : console.log(false)

