
const num = 2;
const numberCube = num**3;
console.log(`The cube of ${num} is ${numberCube}.`);


let address = {
	street:"123 Street",
	barangay: "Bajada", 
	city: "Davao City", 
	country: "Philippines"
	};

const {street, barangay, city, country} = address;
console.log(`I live in ${street}, ${barangay}, ${city}, ${country}!`);


let animal = {
	name: "Lolong",
	type: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
}

const {name, type, weight, measurement} = animal;
console.log(`${name} was a ${type}. He weighed at ${weight} with a measurement of ${measurement}.`);


let numbers = [1,2,3,4,5];
 numbers.forEach(x=> console.log(x));

const initialValue = 0;
const sumWithInitial = numbers.reduce(
	(previousValue, currentValue) => previousValue + currentValue, initialValue
);

console.log(sumWithInitial);


class Dog {
	constructor (name, age, breed) {
		this.name = name,
		this.age = age,
		this.breed = breed
	}
};

const dog1 =  new Dog("Dudai", 1, "labrador");
console.log(dog1);
